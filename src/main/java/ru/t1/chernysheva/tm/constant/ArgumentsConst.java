package ru.t1.chernysheva.tm.constant;

public final class ArgumentsConst {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    public static final String EXIT = "-e";

    private ArgumentsConst() {
    }

}
