package ru.t1.chernysheva.tm;

import ru.t1.chernysheva.tm.constant.ArgumentsConst;
import ru.t1.chernysheva.tm.constant.CommandConst;
import ru.t1.chernysheva.tm.util.TerminalUtil;

public class Application {

    public static void main(String[] args) {
        processArguments(args);
        ProcessCommands();
    }

    private static void ProcessCommands() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private static void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showErrorArgument() {
        System.out.println("[ERROR]");
        System.err.println("The program argument is not correct...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.out.println("[ERROR]");
        System.err.println("Current command is not correct...");
    }

    private static void processCommand(final String argument) {
        switch (argument) {
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.HELP:
                showHelp();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void processArgument(final String argument) {
        switch (argument) {
            case ArgumentsConst.VERSION:
                showVersion();
                break;
            case ArgumentsConst.HELP:
                showHelp();
                break;
            case ArgumentsConst.ABOUT:
                showAbout();
                break;
            case ArgumentsConst.EXIT:
                exit();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Sofia Chernysheva");
        System.out.println("E-mail: schernysheva@t1-consulting.ru");
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s - Show version info.\n", CommandConst.VERSION, ArgumentsConst.VERSION);
        System.out.printf("%s, %s - Show developer info.\n", CommandConst.ABOUT, ArgumentsConst.ABOUT);
        System.out.printf("%s, %s - Show command list.\n", CommandConst.HELP, ArgumentsConst.HELP);
        System.out.printf("%s, %s - Close application.\n", CommandConst.EXIT, ArgumentsConst.EXIT);
    }

}
